#!/bin/bash
usage()
{
cat <<EOF

usage: $0 [options] -t FILE

Run Twitter NER tagger - requires Universal Part of Speech input

OPTIONS:
   -h           Show this message
   -t <FILE>    tag FILE (CoNLL input format: <token>\t<POS-tag>(optional: \t<gold-NER-tag>))
   -o <DIR>     OUTPUT directory (same as file if not specified) [optional] 
   -m <MODEL>   use specified MODEL [optional]
   -s           print to stdout rather than writing to file [optional]
EOF
}
##### DEFAULT VALUES
MODEL=data/ner/models/best/retag15_24-outdir_pooling1000-wsj+gimpel-wiktionary+superimposed/finin+conll.train.lapos.upos.ner.superimposed_tweets.tagged-template3fner-50mpaths2.model.iter27
CLUSTERS=$LOWLANDS_TTAGGER_HOME/data/wordclusters/50mpaths2
OUTDIR=.
STDOUT=
######
while getopts "ho:t:sm:" OPTION
do
    case "$OPTION" in
        h)
            usage
            exit 1
            ;;
        o)
            OUTDIR=$OPTARG
            ;;
        t)
            TESTFILE=$OPTARG
            ;;
	m)
	    MODEL=$OPTARG
	    echo "Use model:" $MODEL
	    ;;
	s)
	    STDOUT=1
	    ;;
    esac
done

if [ -z "$LOWLANDS_TTAGGER_HOME" ]
  then
    echo "Variable LOWLANDS_TTAGGER_HOME is not defined"
    echo "export LOWLANDS_TTAGGER_HOME=`pwd`"
    exit
fi


if [ -z "$TESTFILE" ] 
then
    echo "PARAMETER MISSING!"
    usage
    exit 1
fi



$LOWLANDS_TTAGGER_HOME/data/ner/tag.sh -m $MODEL -o $OUTDIR -c $CLUSTERS -t $TESTFILE    

INTEST=`basename $TESTFILE`

if [ -n "$STDOUT" ] 
then
    cat $OUTDIR/$INTEST.NER-tagged
    rm $OUTDIR/$INTEST.NER-tagged
else
    echo  $OUTDIR/$INTEST.NER-tagged >/dev/stderr
fi