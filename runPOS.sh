#!/bin/bash
usage()
{
cat <<EOF

usage: $0 [options] -t FILE

Run Twitter POS tagger - outputs Universal Part of Speech

OPTIONS:
   -h           Show this message
   -t <FILE>    tag FILE (CoNLL input format: <token>(optional: \t<tag>))
   -o <DIR>     OUTPUT directory (same as file if not specified) [optional] 
   -m <MODEL>   use specified MODEL [optional]
   -s           print to stdout rather than writing to file [optional]
EOF
}
##### DEFAULT VALUES
#MODEL=data/pos/models/best/retag15_24-outdir_pooling1000-wsj-wiktionary+superimposed/ontonotes-wsj-train.cpos.superimposed_tweets.tagged-template3f-50mpaths2.model.iter25
MODEL=data/pos/models/best/retag15_24-outdir_pooling1000-wsj+gimpel-wiktionary+superimposed/ontonotes-wsj-train.cpos+gimpel.train.superimposed_tweets.tagged-template3f-50mpaths2.model.iter27
CLUSTERS=$LOWLANDS_TTAGGER_HOME/data/wordclusters/50mpaths2
OUTDIR=.
STDOUT=
######
while getopts "ho:t:sm:" OPTION
do
    case "$OPTION" in
        h)
            usage
            exit 1
            ;;
        o)
            OUTDIR=$OPTARG
            ;;
        t)
            TESTFILE=$OPTARG
            ;;
	m)
	    MODEL=$OPTARG
	    echo "Use model:" $MODEL
	    ;;
	s)
	    STDOUT=1
	    ;;
    esac
done

if [ -z "$LOWLANDS_TTAGGER_HOME" ]
  then
    echo "Variable LOWLANDS_TTAGGER_HOME is not defined"
    echo "export LOWLANDS_TTAGGER_HOME=`pwd`"
    exit
fi


if [ -z "$TESTFILE" ] 
then
    echo "PARAMETER MISSING!"
    usage
    exit 1
fi



sh $LOWLANDS_TTAGGER_HOME/data/pos/tag.sh -m $MODEL -o $OUTDIR -c $CLUSTERS -t $TESTFILE    

INTEST=`basename $TESTFILE`

if [ -n "$STDOUT" ] 
then
    cat $OUTDIR/$INTEST.tagged
    rm $OUTDIR/$INTEST.tagged
else
    echo  $OUTDIR/$INTEST.tagged >/dev/stderr
fi