#!/bin/bash
usage()
{
cat <<EOF

usage: $0 options

Tag data, extract features, then train

OPTIONS:
   -h      Show this message
   -t      testfile
   -m      POS model
   -o      outdir
   -c      clusters
EOF
}

OUTDIR=.
NERMODEL=
TESTFILE=

while getopts "hc:o:m:t:" OPTION
do
    case "$OPTION" in
        h)
            usage
            exit 1
            ;;
        c)
            CLUSTERS=$OPTARG
            ;;
        o)
            OUTDIR=$OPTARG
            ;;
        m)
            POSMODEL=$OPTARG
            ;;
        t)
            TESTFILE=$OPTARG
            ;;

    esac
done

if [ -z "$LOWLANDS_TTAGGER_HOME" ]
  then
    echo "Variable LOWLANDS_TTAGGER_HOME is not defined"
    exit
fi

if [ -z "$OUTDIR" ] || [ -z "$POSMODEL" ] || [ -z "$TESTFILE" ] || [ -z "$CLUSTERS" ]
then
    echo "PARAMETER MISSING!"
    usage
    exit 1
fi

mkdir -p $OUTDIR

#create features
$LOWLANDS_TTAGGER_HOME/data/pos/createPOSfeats.py $TESTFILE $CLUSTERS | $LOWLANDS_TTAGGER_HOME/data/pos/crfsuite-pos-feats.py > /tmp/$$tmp.feats
#tag file
$LOWLANDS_TTAGGER_HOME/tools/crfsuite-0.12/bin/crfsuite tag -m $POSMODEL /tmp/$$tmp.feats > $OUTDIR/`basename $TESTFILE`.tagged.tmp
#prepare output
paste $OUTDIR/`basename $TESTFILE`.tagged.tmp $TESTFILE | awk '{if (NF==3) printf "%s\t%s\t%s\n",$2,$3,$1 ; else if (NF==2) printf "%s\t%s\n",$2,$1 ; else print }' > $OUTDIR/`basename $TESTFILE`.tagged
#rm temp file
rm $OUTDIR/`basename $TESTFILE`.tagged.tmp

