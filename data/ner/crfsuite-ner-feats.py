#!/usr/bin/env python

"""
An example for part-of-speech tagging.
Copyright 2010,2011 Naoaki Okazaki.
"""

# Separator of field values.
separator = '\t'

# Field names of the input data.
fields = 'w POS cap dig poss hyph single suffix prefix upper let url hash at b2 b4 b6 b8 b10 b12 b14 b16 y'

# Feature template. This template is identical to the one bundled in CRF++
# distribution, but written in a Python object.
templates = (
    (('POS', 0), ),
    (('cap', 0), ),
    (('dig', 0), ),
    (('poss', 0), ),
    (('hyph', 0), ),
    (('single', 0), ),
    (('suffix', 0), ),
    (('prefix', 0), ),
    (('upper', 0), ),
    (('let', 0), ),
    (('url', 0), ),
    (('hash', 0), ),
    (('at', 0), ),
    (('b2', 0), ),
    (('b4', 0), ),
    (('b6', 0), ),
    (('b8', 0), ),
    (('b10', 0), ),
    (('b12', 0), ),
    (('b14', 0), ),
    (('b16', 0), ),
    
    # POS
    (("POS", -2), ),
    (("POS", -1), ),
    (("POS", 0), ),
    (("POS", 1), ),
    (("POS", 2), ),

    # word with current POS
    #U00p:%x[-2,0]/%x[-2,1]
    (('w', -2), ('POS', -2)),
    (('w', -1), ('POS', -1)),
    (('w', 0), ('POS', 0)),
    (('w', 1), ('POS', 1)),
    (('w', 2), ('POS', 2)),

    #new v2
    #cap concat with prefix
    (('cap', -1), ),
    (('cap', -1), ('suffix', 0)),
    (('cap', -1), ('prefix', 0)),
    (('cap', 0), ('suffix', 0)),
    (('cap', 0), ('prefix', 0)),
    #digit
    (('dig', -1), ),
    (('dig', 1), ),
    #hyphen
    (('hyph', -1), ),
    (('hyph', 1), ),
    #single char
    (('single', -1), ),
    (('single', 1), ),    
    #suffix
    (('suffix', -1), ),
    (('suffix', 1), ),
    #prefix
    (('prefix', -1), ),
    (('prefix', 1), ),
    #upper
    (('upper', -1), ),
    (('upper', 1), ),
    #alpha
    (('let', -1), ),
    (('let', 1), ),
    # concat with word only for 2,4,6,8 bit prefix
    (('w',  0), ('b2',  0)),
    (('w',  0), ('b4',  0)),
    (('w',  0), ('b6',  0)),
    (('w',  0), ('b8',  0)),
    #end new v2
    

    (('w',  0), ),
    (('w', -1), ),
    (('w',  1), ),
    (('w', -2), ),
    (('w',  2), ),
    #(('w', -2), ('w',  -1)),
    (('w', -1), ('w',  0)),
    (('w',  0), ('w',  1)),
    #(('w',  1), ('w',  2)),
    #(('w', -2), ('w',  -1), ('w',  0)),
    #(('w', -1), ('w',  0), ('w',  1)),
    #(('w', 0), ('w',  1), ('w',  2)),
    #(('w', -2), ('w',  -1), ('w',  0), ('w',  1)),
    #(('w',  -1), ('w',  0), ('w',  1), ('w', 2)),
    #(('w', -2), ('w',  -1), ('w',  0), ('w',  1), ('w',  2)),

    #(('w',  0), ('w',  -1)),
    #(('w',  0), ('w',  -2)),
    #(('w',  0), ('w',  -3)),
    #(('w',  0), ('w',  -4)),
    #(('w',  0), ('w',  -5)),
    #(('w',  0), ('w',  -6)),
    #(('w',  0), ('w',  -7)),
    #(('w',  0), ('w',  -8)),
    #(('w',  0), ('w',  -9)),

    #(('w',  0), ('w',  1)),
    #(('w',  0), ('w',  2)),
    #(('w',  0), ('w',  3)),
    #(('w',  0), ('w',  4)),
    #(('w',  0), ('w',  5)),
    #(('w',  0), ('w',  6)),
    #(('w',  0), ('w',  7)),
    #(('w',  0), ('w',  8)),
    #(('w',  0), ('w',  9)),
    )


import crfutils

def feature_extractor(X):
    # Apply feature templates to obtain features (in fact, attributes)
    crfutils.apply_templates(X, templates)
    if X:
	# Append BOS and EOS features manually
        X[0]['F'].append('__BOS__')     # BOS feature
        X[-1]['F'].append('__EOS__')    # EOS feature

if __name__ == '__main__':
    crfutils.main(feature_extractor, fields=fields, sep=separator)
