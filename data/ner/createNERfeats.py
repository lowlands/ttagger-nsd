#!/usr/bin/env python
# @author: Barbara Plank
# 
# usage: ./createNERfeats.py FILE [CLUSTERFILE]
import sys
import difflib
import codecs
import re

def main():
    if len(sys.argv) < 2:
        print("specify a file")
        exit()

    clusterfile=None
    if len(sys.argv) == 3:
        clusterfile= sys.argv[2]
        
    #read clusters
    if clusterfile:
        words2ids = {}
        CLUSTER=open(clusterfile)
        for l in CLUSTER:
            l=l.strip()
            fields = l.split("\t")
            clusterId = fields[0]
            word = fields[1]
            words2ids[word] = clusterId
        CLUSTER.close()


    FILE = open(sys.argv[1])
    for l in FILE:
        l = l.strip()
        fields=l.split("\t")
        
        #add dummy if testing 
        if len(fields)==2:
            fields.append("DUMMY")

        #print(fields)
        if fields and len(fields[0]) > 0:
            word = fields[0]
            word = word.replace(" ","_")
            word = word.replace("\t","")

            #replace any number with 0
            word = re.sub(r'\d',"0",word)

            label = fields[-1]
            feats = fields[1:-1]

            # check capitalization
            if word[0].isupper() and not word in ["URL","NUMBER"]:
                feats.append("U+") #first char is uppercase
            else:
                feats.append("u")

            # check if contains digits
            if "0" in word or word == "NUMBER":
                feats.append("D+")
            else:
                feats.append("d")

            # check if contains ' (possesive)
            if "'" in word:
                feats.append("P+")
            else:
                feats.append("p")

            # check if contains hyphen
            if "-" in word:
                feats.append("H+")
            else:
                feats.append("h")

            # single character
            if len(word) == 1:
                feats.append("S+")
            else:
                feats.append("s")

            # 3char suffix
            if len(word) > 4:
                feats.append(word[-3:])
            else:
                feats.append("_")
                
            # 3char prefix
            if len(word) > 4:
                feats.append(word[:3])
            else:
                feats.append(word)

            # if entire word is uppercase
            if word.isupper() and not word in ["URL","NUMBER"]:
                feats.append("UA+") 
            else:
                feats.append("ua")

            # only letters
            if word.isalpha() and not word in ["URL","NUMBER"]:
                feats.append("A+")
            else:
                feats.append("a")

            # TwOrth 
            if word.startswith("http") or word.startswith("URL"):
                feats.append("yUR")
            else:
                feats.append("nUR")

            if word.startswith("#"):
                feats.append("s#")
            else:
                feats.append("n#")

            if word.startswith("@"):
                feats.append("s@")
            else:
                feats.append("n@")

            # brown cluster
            if clusterfile:
                clusterid = words2ids.get(word)
                if clusterid:
                    feats.append(clusterid[0:2]) #prefix 2
                    feats.append(clusterid[0:4]) #prefix 4
                    feats.append(clusterid[0:6]) #prefix 6
                    feats.append(clusterid[0:8]) #prefix 8
                    feats.append(clusterid[0:10]) #prefix 10
                    feats.append(clusterid[0:12]) #prefix 12
                    feats.append(clusterid[0:14]) #prefix 14
                    feats.append(clusterid[0:16]) #prefix 16

                else:
                    feats.append("_")
                    feats.append("_")
                    feats.append("_")
                    feats.append("_")
                    feats.append("_")
                    feats.append("_")
                    feats.append("_")
                    feats.append("_")

            # # gazetters
            # #too slow           if difflib.SequenceMatcher(None, str(word),str(t)).ratio() > 0.75:

            # if dataNAT.get(word):
            #     feats.append("NAT")
            # else:
            #     feats.append("_")

            # if dataLOC.get(word):
            #     feats.append("LOC")
            # else:
            #     feats.append("_")
                
            


            #print(word, label, feats)
            #print(word," ".join(feats),label)
            print "%s\t%s\t%s" % (word,"\t".join(feats),label) 
        else:
            #print emtpy line
            print

    FILE.close()

if __name__=="__main__":
    main()
