#!/bin/bash
usage()
{
cat <<EOF

usage: $0 options

NER tag file

OPTIONS:
   -h      Show this message
   -t      testfile (pos-tagged)
   -m      NER model
   -o      outdir
   -c      clusters
EOF
}

OUTDIR=.
NERMODEL=
TESTFILE=

while getopts "hc:o:m:t:" OPTION
do
    case "$OPTION" in
        h)
            usage
            exit 1
            ;;
        c)
            CLUSTERS=$OPTARG
            ;;
        o)
            OUTDIR=$OPTARG
            ;;
        m)
            NERMODEL=$OPTARG
            ;;
        t)
            TESTFILE=$OPTARG
            ;;

    esac
done

if [ -z "$LOWLANDS_TTAGGER_HOME" ]
  then
    echo "Variable LOWLANDS_TTAGGER_HOME is not defined"
    exit
fi

if [ -z "$OUTDIR" ] || [ -z "$NERMODEL" ] || [ -z "$TESTFILE" ] || [ -z "$CLUSTERS" ]
then
    echo "PARAMETER MISSING!"
    usage
    exit 1
fi

mkdir -p $OUTDIR


$LOWLANDS_TTAGGER_HOME/data/ner/createNERfeats.py $TESTFILE $CLUSTERS | $LOWLANDS_TTAGGER_HOME/data/ner/crfsuite-ner-feats.py > /tmp/$$tmp.feats
$LOWLANDS_TTAGGER_HOME/tools/crfsuite-0.12/bin/crfsuite tag -m $NERMODEL /tmp/$$tmp.feats > $OUTDIR/`basename $TESTFILE`.tagged.tmp
# output file contains just predicted tags, merge:
paste $OUTDIR/`basename $TESTFILE`.tagged.tmp $TESTFILE | awk '{if (NF==4) printf "%s\t%s\t%s\t%s\n",$2,$3,$4,$1 ; else if (NF==3) printf "%s\t%s\t%s\n",$2,$3,$1 ; else print }' > $OUTDIR/`basename $TESTFILE`.NER-tagged
rm $OUTDIR/`basename $TESTFILE`.tagged.tmp

